package org.vaadin.marcus.ui;

import org.junit.Before;
import org.junit.Test;

import java.util.Stack;

import static junit.framework.Assert.*;

/**
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public class NavigateTest {

    private Stack<String> viewStack;
    private Navigate navigate;
    private String navigatedTo;

    @Before
    public void setUp() throws Exception {
        viewStack = new Stack<String>();
        navigate = new Navigate() {
            @Override
            protected Stack<String> getViewStack() {
                return viewStack;
            }

            @Override
            public void to(String viewAndParameters) {
                navigatedTo = viewAndParameters;
            }
        };
    }

    @Test
    public void testNavigatedTo() throws Exception {
        final String view1 = "view1";
        final String view2 = "view2";

        navigate.navigatedTo(view1);
        navigate.navigatedTo(view2);

        assertEquals("View stack contained the wrong number of views", 2, viewStack.size());

        assertEquals("Views returned in incorrect order", view2, viewStack.pop());
        assertEquals("Views returned in incorrect order", view1, viewStack.pop());
    }

    @Test
    public void testCanGoBack() throws Exception {
        viewStack.add("one");

        assertFalse("Navigator allowed navigating back even though it isn't possible", navigate.canGoBack());

        viewStack.add("two");
        assertTrue("Should have been able to navigate back", navigate.canGoBack());
    }

    @Test
    public void testBack() throws Exception {
        viewStack.add("one");
        viewStack.add("two");

        navigate.back();

        assertEquals("Back returned incorrect view", "one", navigatedTo);
    }

    @Test
    public void testThatViewIsNotAddedTwiceToStack() {
        navigate.navigatedTo("one");
        navigate.navigatedTo("one");

        assertEquals("View was added twice", 1, viewStack.size());
    }
}

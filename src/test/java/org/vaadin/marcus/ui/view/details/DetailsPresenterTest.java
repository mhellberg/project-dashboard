package org.vaadin.marcus.ui.view.details;

import org.junit.Before;
import org.junit.Test;
import org.vaadin.marcus.domain.Project;
import org.vaadin.marcus.service.Service;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public class DetailsPresenterTest {

    private DetailsView view;
    private DetailsPresenter presenter;
    private Service service;
    private Project project;

    @Before
    public void setUp() throws Exception {
        view = mock(DetailsView.class);
        presenter = new DetailsPresenter();
        presenter.setView(view);
        service = mock(Service.class);
        project = mock(Project.class);

        presenter.service = service;
    }

    @Test
    public void enteringViewShouldFetchProjectInfo() {
        when(service.findProject(123)).thenReturn(project);

        presenter.viewEntered("123");

        verify(service).updateProjectDetailedStatus(project);
        verify(view).populateProjectInfo(project);

        assertEquals(project, presenter.currentProject);
    }

    @Test
    public void editingShouldGetWHInfo() {
        presenter.currentProject = project;

        presenter.editProjectClicked();

        verify(view).editProject(project);
    }

    @Test
    public void cancelingEditShouldHideEditor() {
        presenter.editCanceled();
        verify(view).hideProjectEditor();
    }

}

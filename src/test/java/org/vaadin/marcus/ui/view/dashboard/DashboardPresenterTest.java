package org.vaadin.marcus.ui.view.dashboard;

import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Test;
import org.vaadin.marcus.domain.Dashboard;
import org.vaadin.marcus.domain.Project;
import org.vaadin.marcus.service.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;
import static org.mockito.Mockito.*;

/**
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public class DashboardPresenterTest {

    private DashboardView view;
    private DashboardPresenter presenter;
    private Dashboard dashboard;
    private Service service;
    private ArrayList<Project> projects;

    @Before
    public void setUp() throws Exception {
        view = mock(DashboardView.class);
        dashboard = mock(Dashboard.class);
        service = mock(Service.class);
        projects = new ArrayList<Project>();

        presenter = new DashboardPresenter();
        presenter.setView(view);
        presenter.service = service;
        presenter.dashboard = dashboard;
        presenter.projects = projects;
    }

    @Test
    public void enteringViewShouldPopulateProjects() {
        final Set<Project> projects = Collections.emptySet();
        final String dashboardName = "test";

        when(dashboard.getProjects()).thenReturn(projects);
        when(service.findOrCreateDashboard(dashboardName)).thenReturn(dashboard);

        presenter.viewEntered(dashboardName);
        verify(service).findOrCreateDashboard(dashboardName);
        verify(service).updateProjectStatus(presenter.projects);
        verify(view).showProjects(presenter.projects);
        assertEquals(dashboard, presenter.dashboard);
    }

    @Test
    public void viewShouldGetNewProjectInstanceOnAdd() {
        presenter.dashboard = null;

        presenter.addProjectClicked();

        verify(view).addNewProject(presenter.currentProject);

    }

    @Test
    public void projectShouldBeAddedToDashboard() {

        Project project = mock(Project.class);
        presenter.currentProject = project;
        when(service.saveDashboard(dashboard)).thenReturn(dashboard);
        presenter.projectAdded();
        verify(dashboard).addProject(project);
        verify(service).updateProjectStatus(project);

        assertNull(presenter.currentProject);
        assertEquals(1, presenter.projects.size());
    }

    @Test
    public void abortingProjectCreationShouldHideEditor() {
        presenter.currentProject = new Project();
        presenter.addProjectAborted();

        verify(view).hideAddProjectEditor();
        assertNull(presenter.currentProject);
    }


    @Test
    public void callingProjectRemoveShouldRemoveProjectFromDashboard() {
        final Project project = new Project();
        presenter.removeProject(project);

        verify(dashboard).removeProject(project);
        verify(view).removeProject(project);
    }

    @Test
    public void exitingWithNonEmptyDashboardShouldCallSave() {
        Set<Project> projectsList = Sets.newHashSet(new Project(), new Project());
        when(dashboard.getProjects()).thenReturn(projectsList);

        presenter.viewExited();

        verify(service).saveDashboard(dashboard);
    }

    @Test
    public void exitingWithNonEmptyDashboardShouldNotCallRemove() {
        final Set<Project> projects = new HashSet<Project>();
        projects.add(mock(Project.class));

        when(dashboard.getProjects()).thenReturn(projects);
        verifyNoMoreInteractions(dashboard);

        presenter.viewExited();
    }

    @Test
    public void exitingWithNullDashboardShouldNotThrowException() {
        presenter.dashboard = null;

        presenter.viewExited();
    }

    @Test
    public void sortingShouldSaveSortSetting() {
        presenter.sortProjects(SortOrder.STATUS);

        verify(presenter.dashboard).setSortOrder(SortOrder.STATUS);
        assertEquals(SortOrder.STATUS, presenter.currentSortOrder);
    }

}

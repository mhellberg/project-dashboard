package org.vaadin.marcus.ui.view.dashboard;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.Locale;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;

/**
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public class JodaDateTimeConverterTest {

    private DateTime time;
    private JodaDateTimeConverter converter;
    private Locale locale;

    @Before
    public void setUp() throws Exception {
        time = new DateTime();
        converter = new JodaDateTimeConverter();
        locale = Locale.US;
    }


    @Test
    public void testTransitivity() {
        assertEquals(time,
                converter.convertToModel(
                        converter.convertToPresentation(time, Date.class, locale)
                        , DateTime.class, locale));
    }

    @Test
    public void testNull() {
        assertNull(converter.convertToModel(null, DateTime.class, locale));
        assertNull(converter.convertToPresentation(null, Date.class, locale));
    }
}

package org.vaadin.marcus.ui;

import com.vaadin.navigator.Navigator;
import com.vaadin.server.Page;
import org.junit.Before;
import org.junit.Test;
import org.vaadin.marcus.ui.view.dashboard.DashboardView;
import org.vaadin.marcus.ui.view.select.SelectDashboardView;

import static org.mockito.Mockito.*;

/**
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public class ProjectDashboardUITest {


    private ProjectDashboardUI ui;
    private Navigator navigator;
    private Page page;

    @Before
    public void setUp() throws Exception {
        navigator = mock(Navigator.class);
        page = mock(Page.class);
        ui = new ProjectDashboardUI() {
            @Override
            public Page getPage() {
                return page;
            }
        };
        ui.navigator = navigator;
    }


    @Test
    public void testNavigateToEmpty() {
        when(page.getUriFragment()).thenReturn("");
        ui.navigateToInitialView();

        verify(navigator).navigateTo(SelectDashboardView.NAME);
    }

    @Test
    public void testNavigateToNull() {
        when(page.getUriFragment()).thenReturn(null);
        ui.navigateToInitialView();

        verify(navigator).navigateTo(SelectDashboardView.NAME);
    }

    @Test
    public void testNavigateToDashboard() {
        when(page.getUriFragment()).thenReturn("!dashboard/dash1");
        ui.navigateToInitialView();

        verify(navigator).navigateTo(DashboardView.NAME + "/dash1");
    }

    @Test
    public void testNavigateToIncorrectDash() {
        when(page.getUriFragment()).thenReturn("!dashboard");
        ui.navigateToInitialView();

        verify(navigator).navigateTo(SelectDashboardView.NAME);
    }
}

package org.vaadin.marcus.domain;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public class ProjectTest {
    private Project project;
    private DateTime startDate;
    private DateTime deadline;

    protected final int DAYS_IN_PROGRESS = 7;
    protected final int DAYS_REMAINING = 3;
    protected final int BUDGET_HOURS = 100;
    protected final int USED_HOURS = 75;

    @Before
    public void setUp() throws Exception {
        project = new Project();

        startDate = new DateTime();
        startDate = startDate.minusDays(DAYS_IN_PROGRESS);

        deadline = new DateTime();
        deadline = deadline.plusDays(DAYS_REMAINING);

        project.setStartDate(startDate.toDate());
        project.setDeadline(deadline.toDate());
        project.setBudgetHours(BUDGET_HOURS);
        project.setUsedHours(USED_HOURS);
    }


    @Test
    public void testHourProgress() {
        assertEquals(0.75, project.getHourProgress(), 1e-3);
    }

    @Test
    public void testStartDate() {
        assertEquals(startDate, project.getStartDateJoda());
    }

    @Test
    public void testDeadline() {
        assertEquals(deadline, project.getDeadlineJoda());
    }

    @Test
    public void testDaysRemaining() {
        assertEquals(DAYS_REMAINING, project.getDaysRemaining());
    }

    @Test
    public void testProjectDuration() {
        assertEquals(DAYS_IN_PROGRESS + DAYS_REMAINING, project.getProjectDuration());
    }

    @Test
    public void testTimeProgress() {
        assertEquals(0.7, project.getTimeProgress(), 1e-3);
    }

    @Test
    public void testOKStatus() {
        project.setUsedHours(60);
        assertEquals(Project.Status.OK, project.getStatus());
    }

    @Test
    public void testWarningStatus() {
        project.setUsedHours(75);
        assertEquals(Project.Status.WARNING, project.getStatus());
    }

    @Test
    public void testProblemStatus() {
        project.setUsedHours(90);
        assertEquals(Project.Status.PROBLEM, project.getStatus());
    }

    @Test
    public void testTooManyHours() {
        project.setUsedHours(200);
        assertEquals(Project.Status.PROBLEM, project.getStatus());
    }

    @Test
    public void testTimeExceeded() {
        deadline = new DateTime();
        deadline = deadline.minusDays(1);
        project.setDeadline(deadline.toDate());

        assertEquals(Project.Status.PROBLEM, project.getStatus());
    }
}

package org.vaadin.marcus.domain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import static org.junit.Assert.*;

/**
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public class RepositoryTest {
    EntityManager em;
    private Repository repository;

    @Before
    public void setUp() {
        em = Persistence.createEntityManagerFactory("test").createEntityManager();
        em.getTransaction().begin();
        repository = new Repository();
        repository.em = em;
    }


    @Test
    public void testSave() {
        assertEquals("Database was not empty at the start", 0, getDashboardCount());
        Dashboard dashboard = new Dashboard();
        Dashboard saved = repository.save(dashboard);
        assertTrue(saved.isPersistent());
        assertEquals("Dashboard was not persisted", 1, getDashboardCount());
    }

    @Test
    public void testSaveWithProject() {

        Dashboard dashboard = new Dashboard();
        dashboard.addProject(new Project());
        Dashboard saved = repository.save(dashboard);

        assertEquals(1, saved.getProjects().size());
    }

    @Test
    public void testDeleteWithProject() {
        Dashboard dashboard = new Dashboard();
        dashboard.addProject(new Project());
        Dashboard saved = repository.save(dashboard);

        Long dashboardId = saved.getId();
        Long projectId = saved.getProjects().iterator().next().getId();

        repository.removeDashboard(dashboard);

        assertNull(repository.findDashboard(dashboardId));
        assertNull(repository.findProject(projectId));

    }

    private int getDashboardCount() {
        return ((Number) em.createQuery("select count(d) from Dashboard d").getSingleResult()).intValue();
    }


    @After
    public void tearDown() {
        if (em != null) {
            em.getTransaction().rollback();
        }
    }
}

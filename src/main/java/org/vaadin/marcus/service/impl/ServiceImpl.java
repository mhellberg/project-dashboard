package org.vaadin.marcus.service.impl;

import com.google.common.base.Joiner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaadin.marcus.domain.Dashboard;
import org.vaadin.marcus.domain.EmployeeInfo;
import org.vaadin.marcus.domain.Project;
import org.vaadin.marcus.domain.Repository;
import org.vaadin.marcus.service.Service;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
@Stateless
public class ServiceImpl implements Service {

    Logger logger = LoggerFactory.getLogger(ServiceImpl.class);

    @Resource(name = "jdbc/wh")
    DataSource dataSource;

    @Inject
    Repository repository;

    @Override
    public Dashboard findDashboard(String dashboardName) {
        return repository.findDashdobard(dashboardName);
    }

    @Override
    public void removeDashboard(Dashboard dashboard) {
        repository.removeDashboard(dashboard);
    }

    @Override
    public void updateProjectStatus(Collection<Project> projects) {
        for (Project project : projects) {
            updateProjectStatus(project);
        }
    }

    @Override
    public Dashboard findOrCreateDashboard(String dashboardName) {
        logger.debug("Finding dashboard with name {}", dashboardName);

        Dashboard dashboard = findDashboard(dashboardName);
        if (dashboard == null) {
            dashboard = new Dashboard();
            dashboard.setDashboardName(dashboardName);
        }
        return dashboard;
    }


    @Override
    public Project findProject(long id) {
        return repository.findProject(id);
    }

    @Override
    public Dashboard saveDashboard(Dashboard dashboard) {
        return repository.save(dashboard);
    }

    @Override
    public Project saveProject(Project project) {
        return repository.save(project);
    }

    public void updateProjectStatus(Project project) {
        logger.debug("Updating project status for {}", project.getName());

        Set<Integer> contractIds = project.getContractIds();

        String statement = "select sum(h.hours) " +
                "as hours " +
                "from hour as h " +
                "where h.project_id=? ";

        if (!contractIds.isEmpty()) {
            statement += "and h.hour_type_id in (?)";
        }

        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            final PreparedStatement preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setInt(1, project.getProjectId());

            if (!contractIds.isEmpty()) {
                preparedStatement.setString(2, Joiner.on(", ").join(contractIds));
            }

            final ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                project.setUsedHours(resultSet.getInt("hours"));
            }
        } catch (SQLException e) {
            logger.error("Error updating project status");
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error("Failed to close DB connection", e);
                }
            }
        }
    }


    public void updateProjectDetailedStatus(Project project) {

        logger.debug("Updating detailed status for project {}", project.getName());

        getEmployeeContributions(project);
    }

    private void getEmployeeContributions(Project project) {
        Set<Integer> contractIds = project.getContractIds();
        String statement = "select " +
                "e.name as name, " +
                "sum(h.hours) as hours, " +
                "max(h.date) as latestEntry " +
                "from " +
                "hour as h, " +
                "employee as e " +
                "where h.employee_id=e.employee_id " +
                "and h.project_id=? ";

        if (!contractIds.isEmpty()) {
            statement += "and h.hour_type_id in (?) ";
        }

        statement += "group by e.name " +
                "order by hours desc";

        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            final PreparedStatement preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setInt(1, project.getProjectId());

            if (!contractIds.isEmpty()) {
                preparedStatement.setString(2, Joiner.on(", ").join(contractIds));
            }

            final ResultSet resultSet = preparedStatement.executeQuery();

            List<EmployeeInfo> employeeInfoList = new LinkedList<EmployeeInfo>();
            while (resultSet.next()) {
                employeeInfoList.add(new EmployeeInfo(resultSet.getString("name"), resultSet.getDouble("hours"), resultSet.getDate("latestEntry")));
            }

            project.setEmployeeInfo(employeeInfoList);

        } catch (SQLException e) {
            logger.error("Error getting employee contributions", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error("Failed to close DB connection", e);
                }
            }
        }
    }


}

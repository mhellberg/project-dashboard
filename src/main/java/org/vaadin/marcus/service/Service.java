package org.vaadin.marcus.service;

import org.vaadin.marcus.domain.Dashboard;
import org.vaadin.marcus.domain.Project;

import java.util.Collection;

/**
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public interface Service {

    /**
     * Finds dashboard by name.
     *
     * @param username Username
     * @return Found Dashboard or null
     */
    public Dashboard findDashboard(String username);

    /**
     * @param dashboardName Dashboard name
     * @return existing dashboard if found, else new dashboard
     */
    public Dashboard findOrCreateDashboard(String dashboardName);

    /**
     * Removes given dashboard from persistent storage.
     *
     * @param dashboard
     */
    public void removeDashboard(Dashboard dashboard);

    /**
     * Updates the status for a list of project from underlying data source.
     *
     * @param projects
     */
    public void updateProjectStatus(Collection<Project> projects);

    /**
     * Updates detailed status to project. This is a more expensive operation
     * than updateProjectStatus(), so call it only if really needed.
     *
     * @param project
     */
    public void updateProjectDetailedStatus(Project project);

    /**
     * Updates the status for a project from an underlying data source.
     *
     * @param project
     */
    public void updateProjectStatus(Project project);


    /**
     * @param id Project identifier
     * @return found project or null
     */
    public Project findProject(long id);

    /**
     * Saves given dashboard.
     *
     * @param dashboard dashboard to update
     * @return updated dashboard
     */
    public Dashboard saveDashboard(Dashboard dashboard);

    /**
     * Saves changes to project
     *
     * @param currentProject
     * @return
     */
    Project saveProject(Project currentProject);
}

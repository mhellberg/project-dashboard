package org.vaadin.marcus.ui;

import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Widgetset;
import com.vaadin.cdi.CDIUI;
import com.vaadin.cdi.CDIViewProvider;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.UI;
import org.vaadin.marcus.service.Service;
import org.vaadin.marcus.ui.view.DashboardChangedEvent;
import org.vaadin.marcus.ui.view.dashboard.DashboardView;
import org.vaadin.marcus.ui.view.details.DetailsView;
import org.vaadin.marcus.ui.view.select.SelectDashboardView;
import org.vaadin.marcus.util.Lang;

import javax.enterprise.event.Observes;
import javax.inject.Inject;

/**
 * The Application's "main" class
 */
@SuppressWarnings("serial")
@CDIUI
@Widgetset("org.vaadin.marcus.ProjectDashboardWidgetSet")
@Theme(DashboardTheme.THEME_NAME)
@Push
public class ProjectDashboardUI extends UI {

    @Inject
    CDIViewProvider viewProvider;

    @Inject
    Service service;

    @Inject
    Navigate navigate;

    protected Navigator navigator;

    @Override
    protected void init(VaadinRequest request) {
        getPage().setTitle(Lang.get("project.name"));
        createLayouts();
        navigateToInitialView();
        setLocale(request.getLocale());
    }

    private void createLayouts() {
        final CssLayout rootLayout = new CssLayout();
        rootLayout.addStyleName(DashboardTheme.ROOT_LAYOUT);
        rootLayout.setSizeUndefined();

        final CssLayout contentLayout = new CssLayout();
        contentLayout.setSizeUndefined();
        contentLayout.addStyleName(DashboardTheme.CONTENT_LAYOUT);

        rootLayout.addComponent(new HeaderLayout());
        rootLayout.addComponent(contentLayout);
        setContent(rootLayout);

        navigator = new Navigator(this, contentLayout);
        navigator.addProvider(viewProvider);
        navigator.setErrorView(ErrorView.class);
        navigator.addViewChangeListener(updateViewStackListener());
    }

    /**
     * Try to prevent incorrect URIs by only navigating to known patterns on init.
     */
    protected void navigateToInitialView() {
        final String uriFragment = getPage().getUriFragment();

        if (uriFragment != null &&
                (uriFragment.matches("!" + DashboardView.NAME + "/\\w+") ||
                        uriFragment.matches("!" + DetailsView.NAME + "/\\d+"))) {
            navigator.navigateTo(uriFragment.substring(1));
        } else {
            navigator.navigateTo(SelectDashboardView.NAME);
        }
    }

    public void dashboardChanged(@Observes DashboardChangedEvent dashboardChangedEvent) {
        navigator.navigateTo(DashboardView.NAME + "/" + dashboardChangedEvent.getDashboardName());
    }

    private ViewChangeListener updateViewStackListener() {
        return new ViewChangeListener() {
            @Override
            public boolean beforeViewChange(ViewChangeEvent event) {
                navigate.navigatedTo(event.getViewName() + "/" + event.getParameters());
                return true;
            }

            @Override
            public void afterViewChange(ViewChangeEvent event) {

            }
        };
    }
}

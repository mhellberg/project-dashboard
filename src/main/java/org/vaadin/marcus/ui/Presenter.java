package org.vaadin.marcus.ui;

import com.vaadin.navigator.View;
import org.vaadin.marcus.service.Service;

import javax.ejb.EJB;

/**
 * Base presenter implementation that takes care of common presenter functionality.
 *
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public abstract class Presenter {
    protected View view;

    @EJB
    public Service service;

    /**
     * Sets the view that should be bound to this presenter.
     *
     * @param view
     */
    public void setView(View view) {
        this.view = view;
    }

    /**
     * View has been shown. Can be used to provide view with initial data.
     *
     * @param parameters
     */
    public abstract void viewEntered(String parameters);

    /**
     * Can be used to perform cleanup actions.
     */
    public abstract void viewExited();

    /**
     * Subclasses should override to return their specific View type.
     *
     * @return
     */
    public View getView() {
        return view;
    }
}

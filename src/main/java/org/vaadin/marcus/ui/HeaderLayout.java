package org.vaadin.marcus.ui;

import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import org.vaadin.marcus.util.Lang;

/**
 * Main layout header.
 *
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public class HeaderLayout extends CssLayout {

    public HeaderLayout() {
        setSizeUndefined();
        addStyleName(DashboardTheme.HEADER_LAYOUT);

        final Label logoLabel = new Label(Lang.get("project.name"));
        logoLabel.addStyleName(DashboardTheme.LOGO_LABEL);
        logoLabel.addStyleName(DashboardTheme.LABEL_H1);
        logoLabel.setSizeUndefined();

        addComponent(logoLabel);
    }
}

package org.vaadin.marcus.ui.view.dashboard;

import com.vaadin.data.util.converter.Converter;
import org.joda.time.DateTime;

import java.util.Date;
import java.util.Locale;

/**
 * Converter for handling JodaTime dates in DateFields.
 *
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public class JodaDateTimeConverter implements Converter<Date, DateTime> {

    @Override
    public DateTime convertToModel(Date date, Class<? extends DateTime> targetType, Locale locale) throws ConversionException {
        if (date == null) {
            return null;
        }
        return new DateTime(date.getTime());
    }

    @Override
    public Date convertToPresentation(DateTime dateTime, Class<? extends Date> targetType, Locale locale) throws ConversionException {
        if (dateTime == null) {
            return null;
        }

        return new Date(dateTime.getMillis());
    }

    @Override
    public Class<DateTime> getModelType() {
        return DateTime.class;
    }

    @Override
    public Class<Date> getPresentationType() {
        return Date.class;
    }
}

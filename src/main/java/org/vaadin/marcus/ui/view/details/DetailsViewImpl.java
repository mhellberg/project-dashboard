package org.vaadin.marcus.ui.view.details;

import com.vaadin.cdi.CDIView;
import com.vaadin.event.ShortcutAction;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.ui.*;
import org.vaadin.marcus.domain.Project;
import org.vaadin.marcus.ui.DashboardTheme;
import org.vaadin.marcus.ui.Navigate;
import org.vaadin.marcus.ui.components.StatusGrid;
import org.vaadin.marcus.ui.view.select.SelectDashboardView;
import org.vaadin.marcus.util.Lang;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * Default implementation for {@link DetailsView}
 *
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
@CDIView(DetailsView.NAME)
public class DetailsViewImpl extends CssLayout implements DetailsView {

    private VerticalLayout contentWrapper;
    @Inject
    DetailsPresenter presenter;

    @Inject
    Navigate navigate;

    private Button editButton;
    private EditProjectLayout editProjectLayout;
    private HorizontalLayout headerLayout;
    private Button backButton;
    private NotesLayout notesLayout;

    public DetailsViewImpl() {
        addStyleName(DashboardTheme.Details.VIEW_NAME);
    }

    private void createLayouts() {
        addHeader();
        contentWrapper = new VerticalLayout();
        contentWrapper.setSpacing(true);
        contentWrapper.setMargin(true);
        addComponent(contentWrapper);
        addShortcuts();
    }

    private void addHeader() {
        headerLayout = new HorizontalLayout();
        headerLayout.setWidth("100%");
        headerLayout.addStyleName(DashboardTheme.Details.HEADER);

        if (navigate.canGoBack()) {
            backButton = new Button(Lang.get("details.header.backToDash"), new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    navigate.back();
                }
            });
            backButton.setDescription(Lang.get("details.header.backToDash.description"));
            headerLayout.addComponent(backButton);
        }

        editButton = new Button(Lang.get("details.editProject"), new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                presenter.editProjectClicked();
            }
        });
        editButton.setDescription(Lang.get("details.editProject.description"));
        headerLayout.addComponent(editButton);
        headerLayout.setComponentAlignment(editButton, Alignment.BOTTOM_RIGHT);

        addComponent(headerLayout);
    }

    @PostConstruct
    public void init() {
        presenter.setView(this);
    }


    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        removeAllComponents();
        createLayouts();
        presenter.viewEntered(event.getParameters());
    }

    @Override
    public void populateProjectInfo(Project project) {
        Page.getCurrent().setTitle(Lang.get("details.title", project.getName()));

        contentWrapper.removeAllComponents();
        addBasicStatus(project);

        if (!project.getEmployeeInfo().isEmpty()) {
            contentWrapper.addComponent(new EmployeeContributionsLayout(project.getEmployeeInfo()));
        }

        notesLayout = new NotesLayout(project, presenter);
        contentWrapper.addComponent(notesLayout);
    }

    @Override
    public void editProject(Project currentProject) {
        editButton.setEnabled(false);
        disableShortcuts();
        editProjectLayout = new EditProjectLayout(currentProject, presenter);
        addComponent(editProjectLayout, getComponentIndex(headerLayout) + 1);
    }

    @Override
    public void hideProjectEditor() {
        if (editProjectLayout != null) {
            removeComponent(editProjectLayout);
            editProjectLayout = null;
        }

        editButton.setEnabled(true);
        addShortcuts();
    }

    @Override
    public void editNotes(String notes) {
        disableShortcuts();
        notesLayout.editContent(notes);
    }

    @Override
    public void showNotes(String notes) {
        addShortcuts();
        notesLayout.showNotes(notes);
    }

    @Override
    public void showProjectNotFound() {
        Notification.show(Lang.get("details.project.not.found"));
        UI.getCurrent().getNavigator().navigateTo(SelectDashboardView.NAME);
    }

    private void disableShortcuts() {
        editButton.removeClickShortcut();
        if (backButton != null) {
            backButton.removeClickShortcut();
        }
    }

    private void addShortcuts() {
        editButton.setClickShortcut(ShortcutAction.KeyCode.E);
        if (backButton != null) {
            backButton.setClickShortcut(ShortcutAction.KeyCode.B);
        }
        UI.getCurrent().focus();
    }

    private void addBasicStatus(Project project) {
        final Label nameLabel = new Label(project.getName());
        nameLabel.addStyleName(DashboardTheme.Details.PROJECT_NAME);
        nameLabel.addStyleName(DashboardTheme.LABEL_H1);
        contentWrapper.addComponent(nameLabel);

        contentWrapper.addComponent(new StatusGrid(project));
    }
}

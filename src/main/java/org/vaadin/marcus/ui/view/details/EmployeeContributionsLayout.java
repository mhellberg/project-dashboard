package org.vaadin.marcus.ui.view.details;

import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;
import org.vaadin.marcus.domain.EmployeeInfo;
import org.vaadin.marcus.ui.DashboardTheme;
import org.vaadin.marcus.util.Lang;

import java.util.List;

/**
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public class EmployeeContributionsLayout extends VerticalLayout implements Button.ClickListener {


    private static final String DOWN_STYLE = "down";
    private List<EmployeeInfo> employeeInfo;
    private Button chartButton;
    private Button tableButton;

    public EmployeeContributionsLayout(List<EmployeeInfo> employeeInfo) {
        this.employeeInfo = employeeInfo;

        setWidth("100%");
        setSpacing(true);

        final Label contributionsHeadingLabel = new Label(Lang.get("details.contributionByEmployee"));
        contributionsHeadingLabel.addStyleName(DashboardTheme.LABEL_H2);

        addComponent(contributionsHeadingLabel);

        addTabSheet(employeeInfo);
    }

    private void addTabSheet(List<EmployeeInfo> employeeInfo) {
        TabSheet tabSheet = new TabSheet();
        tabSheet.setWidth("100%");
        tabSheet.setHeight("400px");
        tabSheet.addStyleName(DashboardTheme.TABSHEET_MINIMAL);

        tabSheet.addTab(new EmployeeContributionChart(employeeInfo), Lang.get("details.chart"));
        tabSheet.addTab(new EmployeeContributionsTable(employeeInfo), Lang.get("details.table"));

        addComponent(tabSheet);
    }


    @Override
    public void buttonClick(Button.ClickEvent event) {

    }
}

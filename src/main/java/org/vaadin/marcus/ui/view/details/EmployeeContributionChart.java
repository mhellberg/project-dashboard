package org.vaadin.marcus.ui.view.details;

import com.vaadin.addon.charts.Chart;
import com.vaadin.addon.charts.model.ChartType;
import com.vaadin.addon.charts.model.Configuration;
import com.vaadin.addon.charts.model.DataSeries;
import com.vaadin.addon.charts.model.DataSeriesItem;
import org.vaadin.marcus.domain.EmployeeInfo;
import org.vaadin.marcus.ui.DashboardTheme;

import java.util.List;

/**
 * PieChart that displays the work division in the project.
 *
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public class EmployeeContributionChart extends Chart {

    public EmployeeContributionChart(List<EmployeeInfo> employeesToHours) {
        super(ChartType.PIE);
        addStyleName(DashboardTheme.Details.CONTRIBUTION_CHART);
        setSizeFull();

        final DataSeries dataSeries = new DataSeries();
        for (EmployeeInfo entry : employeesToHours) {
            dataSeries.add(new DataSeriesItem(entry.getEmployeeName(), entry.getHours()));
        }

        final Configuration configuration = getConfiguration();
        configuration.addSeries(dataSeries);
        configuration.setTitle("");
        drawChart(configuration);
    }
}

package org.vaadin.marcus.ui.view.select;

import com.vaadin.cdi.CDIView;
import com.vaadin.event.ShortcutAction;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;
import org.vaadin.marcus.ui.DashboardTheme;
import org.vaadin.marcus.ui.view.DashboardChangedEvent;
import org.vaadin.marcus.util.Lang;

import javax.inject.Inject;

/**
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
@CDIView(SelectDashboardView.NAME)
public class SelectDashboardViewImpl extends VerticalLayout implements SelectDashboardView {

    private TextField dashboardNameField;

    @Inject
    javax.enterprise.event.Event<DashboardChangedEvent> dashboardChangedEventEvent;

    public SelectDashboardViewImpl() {
        setMargin(true);
        addStyleName(DashboardTheme.Select.VIEW_STYLE);


        final VerticalLayout wrapperLayout = createWrapper();

        addComponent(wrapperLayout);
        setComponentAlignment(wrapperLayout, Alignment.MIDDLE_CENTER);
    }

    private VerticalLayout createWrapper() {
        final VerticalLayout wrapperLayout = new VerticalLayout();
        wrapperLayout.addStyleName(DashboardTheme.Select.WRAPPER_LAYOUT);
        wrapperLayout.setSpacing(true);
        wrapperLayout.setMargin(true);
        wrapperLayout.setSizeUndefined();

        final Label headerLabel = new Label(Lang.get("select.selectDashboard"));
        headerLabel.addStyleName(DashboardTheme.LABEL_H2);
        headerLabel.setSizeUndefined();
        wrapperLayout.addComponent(headerLabel);

        final Label helpLabel = new Label(Lang.get("select.helpText"), ContentMode.HTML);
        helpLabel.addStyleName(DashboardTheme.Select.HELP_LABEL);
        wrapperLayout.addComponent(helpLabel);

        final HorizontalLayout formWrapper = new HorizontalLayout();
        formWrapper.addStyleName(DashboardTheme.Select.FORM_WRAPPER);
        formWrapper.setSpacing(true);

        dashboardNameField = new TextField();

        final Button showButton = new Button(Lang.get("select.show"), new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                dashboardChangedEventEvent.fire(new DashboardChangedEvent(dashboardNameField.getValue()));
            }
        });
        showButton.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        showButton.addStyleName(DashboardTheme.BUTTON_SMALL);
        showButton.addStyleName(DashboardTheme.BUTTON_DEFAULT);

        formWrapper.addComponents(dashboardNameField, showButton);
        formWrapper.setComponentAlignment(dashboardNameField, Alignment.MIDDLE_LEFT);
        formWrapper.setComponentAlignment(showButton, Alignment.MIDDLE_RIGHT);

        wrapperLayout.addComponent(formWrapper);
        return wrapperLayout;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        dashboardNameField.focus();
    }
}

package org.vaadin.marcus.ui.view.details;

import com.vaadin.addon.timeline.Timeline;
import com.vaadin.data.util.IndexedContainer;
import org.vaadin.marcus.domain.DailyEffort;
import org.vaadin.marcus.ui.DashboardTheme;
import org.vaadin.marcus.util.Lang;

import java.awt.*;
import java.util.List;

/**
 * Timeline chart showing work effort by day.
 *
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public class DailyEffortChart extends Timeline {

    @SuppressWarnings("unchecked")
    public DailyEffortChart(List<DailyEffort> dateToHours) {
        addStyleName(DashboardTheme.Details.DAILY_EFFORT_TIMELINE);
        setWidth("100%");

        final IndexedContainer container = createIndexedContainer();

        for (DailyEffort dateToHour : dateToHours) {
            final Object itemId = container.addItem();
            container.getContainerProperty(itemId, Timeline.PropertyId.TIMESTAMP).setValue(dateToHour.getDate());
            container.getContainerProperty(itemId, Timeline.PropertyId.VALUE).setValue(new Float(dateToHour.getHours()));
        }

        container.sort(new Object[]{Timeline.PropertyId.TIMESTAMP}, new boolean[]{true});

        addGraphDataSource(container, Timeline.PropertyId.TIMESTAMP, Timeline.PropertyId.VALUE);
        setGraphOutlineColor(container, new Color(0x00, 0xb4, 0xf0));
        setZoomLevelsVisible(false);
        setVerticalAxisLegendUnit(container, Lang.get("details.hours"));
    }

    private IndexedContainer createIndexedContainer() {
        IndexedContainer container = new IndexedContainer();
        container.addContainerProperty(Timeline.PropertyId.VALUE, Float.class,
                new Float(0));
        container.addContainerProperty(Timeline.PropertyId.TIMESTAMP,
                java.util.Date.class, null);
        return container;
    }
}

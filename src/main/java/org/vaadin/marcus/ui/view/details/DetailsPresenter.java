package org.vaadin.marcus.ui.view.details;

import org.vaadin.marcus.domain.Project;
import org.vaadin.marcus.service.Service;
import org.vaadin.marcus.ui.Presenter;

import javax.ejb.EJB;

/**
 * Presenter for {@link DetailsView}s
 *
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public class DetailsPresenter extends Presenter {

    @EJB
    Service service;
    Project currentProject;

    @Override
    public DetailsView getView() {
        return (DetailsView) super.getView();
    }

    @Override
    public void viewEntered(String parameters) {
        currentProject = service.findProject(Long.parseLong(parameters));
        if (currentProject != null) {
            service.updateProjectDetailedStatus(currentProject);
            getView().populateProjectInfo(currentProject);
        } else {
            getView().showProjectNotFound();
        }
    }

    @Override
    public void viewExited() {

    }

    public void editProjectClicked() {
        getView().editProject(currentProject);
    }

    public void projectSaved() {
        getView().hideProjectEditor();
        currentProject = service.saveProject(currentProject);
        service.updateProjectStatus(currentProject);
        service.updateProjectDetailedStatus(currentProject);
        getView().populateProjectInfo(currentProject);
    }

    public void editCanceled() {
        getView().hideProjectEditor();
    }

    public void editNotesClicked() {
        getView().editNotes(currentProject.getNotes());
    }

    public void notesSaved(String notes) {
        currentProject.setNotes(notes);
        getView().showNotes(notes);
    }

    public void noteEditCanceled() {
        getView().showNotes(currentProject.getNotes());
    }
}

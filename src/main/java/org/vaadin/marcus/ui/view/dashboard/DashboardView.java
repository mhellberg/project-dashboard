package org.vaadin.marcus.ui.view.dashboard;

import com.vaadin.navigator.View;
import org.vaadin.marcus.domain.Project;

import java.util.Collection;

/**
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public interface DashboardView extends View {
    public static final String NAME = "dashboard";

    void addNewProject(Project project);

    void showProjects(Collection<Project> projects);

    void hideAddProjectEditor();

    void removeProject(Project project);

    void showDashboardNameError();

    void setSortOrder(SortOrder sortOrder);

    void setDashboardName(String dashboardName);

}

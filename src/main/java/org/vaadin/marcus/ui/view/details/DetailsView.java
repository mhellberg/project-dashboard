package org.vaadin.marcus.ui.view.details;

import com.vaadin.navigator.View;
import org.vaadin.marcus.domain.Project;

/**
 * Interface for the project details view.
 *
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public interface DetailsView extends View {
    public static final String NAME = "details";

    void populateProjectInfo(Project project);

    void editProject(Project currentProject);

    void hideProjectEditor();

    void editNotes(String notes);

    void showNotes(String notes);

    void showProjectNotFound();
}

package org.vaadin.marcus.ui.view.dashboard;

/**
 * Enum for different sort orders.
 *
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public enum SortOrder {
    DEADLINE, STATUS
}

package org.vaadin.marcus.ui.view.dashboard;

import com.vaadin.ui.Button;
import com.vaadin.ui.NativeButton;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.vaadin.marcus.domain.Project;
import org.vaadin.marcus.ui.DashboardTheme;
import org.vaadin.marcus.ui.Navigate;
import org.vaadin.marcus.ui.components.ConfirmDialog;
import org.vaadin.marcus.ui.components.StatusGrid;
import org.vaadin.marcus.ui.view.details.DetailsView;
import org.vaadin.marcus.util.Lang;

/**
 * Status layout for one single project in the dashboard.
 *
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public class ProjectStatusLayout extends VerticalLayout {
    private final Project project;
    private final DashboardPresenter presenter;
    private Navigate navigate;

    public ProjectStatusLayout(Project project, DashboardPresenter presenter, Navigate navigate) {
        this.project = project;
        this.presenter = presenter;
        this.navigate = navigate;

        addStyleName(DashboardTheme.Dashboard.PROJECT_STATUS_LAYOUT);
        addStyleName(project.getStatus().name().toLowerCase());

        addName();
        addDeleteButton();
        addComponent(new StatusGrid(project));

    }

    private void addDeleteButton() {
        final NativeButton deleteButton = new NativeButton("x");
        deleteButton.addStyleName(DashboardTheme.Dashboard.DELETE_BUTTON);
        deleteButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                final ConfirmDialog confirmDialog = new ConfirmDialog(Lang.get("dashboard.project.confirmDelete"), Lang.get("dashboard.project.areYouSure"), new Runnable() {
                    @Override
                    public void run() {
                        presenter.removeProject(project);
                    }
                });

                UI.getCurrent().addWindow(confirmDialog);
            }
        });

        addComponent(deleteButton);
    }

    private void addName() {
        final NativeButton nameButton = new NativeButton(project.getName(), new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                navigate.to(DetailsView.NAME + "/" + project.getId());
            }
        });
        nameButton.addStyleName(DashboardTheme.Dashboard.PROJECT_NAME);
        addComponent(nameButton);
    }
}

package org.vaadin.marcus.ui.view.dashboard;

import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import org.vaadin.marcus.domain.Project;
import org.vaadin.marcus.ui.DashboardTheme;
import org.vaadin.marcus.ui.components.ProjectForm;
import org.vaadin.marcus.util.Lang;

/**
 * Form for adding new projects to a dashboard.
 *
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public class AddProjectLayout extends VerticalLayout {


    private ProjectForm projectForm;
    private DashboardPresenter presenter;

    public AddProjectLayout(Project project, DashboardPresenter presenter) {
        this.presenter = presenter;

        addStyleName(DashboardTheme.Dashboard.ADD_PROJECT_LAYOUT);
        addStyleName(DashboardTheme.INLINE_LAYOUT);

        setSpacing(true);

        addHelpText();
        addForm(project);
    }

    private void addForm(Project project) {
        final ProjectForm.Config config = new ProjectForm.Config();
        config.setProject(project);
        config.setCancelListener(cancelListener);
        config.setCancelButtonCaption(Lang.get("dashboard.add.cancelButton"));
        config.setOkButtonListener(addListener);
        config.setOkButtonCaption(Lang.get("dashboard.add.addButton"));
        config.setCommitFailMessage(Lang.get("dashboard.add.addFailed"));
        projectForm = new ProjectForm(config);
        addComponent(projectForm);
    }


    private void addHelpText() {
        final Label helpTextLabel = new Label(Lang.get("dashboard.add.helptext"));
        helpTextLabel.addStyleName(DashboardTheme.Dashboard.ADD_WINDOW_HELP_LABEL);
        addComponent(helpTextLabel);
    }

    private Button.ClickListener cancelListener = new Button.ClickListener() {
        @Override
        public void buttonClick(Button.ClickEvent event) {
            presenter.addProjectAborted();
        }
    };

    private Button.ClickListener addListener = new Button.ClickListener() {
        @Override
        public void buttonClick(Button.ClickEvent event) {
            if (projectForm.commit()) {
                presenter.projectAdded();
            }
        }
    };
}

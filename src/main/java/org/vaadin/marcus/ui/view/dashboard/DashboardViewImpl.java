package org.vaadin.marcus.ui.view.dashboard;

import com.vaadin.cdi.CDIView;
import com.vaadin.data.Property;
import com.vaadin.event.ShortcutAction;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.ui.*;
import org.vaadin.marcus.domain.Project;
import org.vaadin.marcus.ui.DashboardTheme;
import org.vaadin.marcus.ui.Navigate;
import org.vaadin.marcus.ui.view.select.SelectDashboardView;
import org.vaadin.marcus.util.Lang;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Default implementation for DashboardView.
 *
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
@CDIView(DashboardView.NAME)
public class DashboardViewImpl extends CssLayout implements DashboardView {

    @Inject
    DashboardPresenter presenter;

    @Inject
    Navigate navigate;

    private AddProjectLayout addProjectLayout;
    private Button addProjectButton;
    private CssLayout projectsLayout;
    private HorizontalLayout headerLayout;


    private Map<Project, Layout> projectLayoutMap = new HashMap<Project, Layout>();
    private ComboBox sortSelect;
    private TextField dashboardNameEditor;

    public DashboardViewImpl() {
        addStyleName(DashboardTheme.Dashboard.VIEW_STYLE);

        addHeader();
        addProjectsLayout();
        addShortcuts();
    }

    @PostConstruct
    void init() {
        presenter.setView(this);
    }

    private void addHeader() {
        dashboardNameEditor = new TextField();
        dashboardNameEditor.addStyleName(DashboardTheme.Dashboard.DASHBOARD_NAME_EDITOR);
        dashboardNameEditor.setImmediate(true);
        dashboardNameEditor.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent dashboardNameEditorevent) {
                disableShortcuts();
                presenter.renameDashboard(dashboardNameEditor.getValue());
                UI.getCurrent().focus();
            }
        });
        addComponent(dashboardNameEditor);

        headerLayout = new HorizontalLayout();
        headerLayout.setWidth("100%");
        headerLayout.addStyleName(DashboardTheme.Dashboard.HEADER);

        addProjectButton = new Button(Lang.get("dashboard.addProject"), new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                presenter.addProjectClicked();
            }
        });
        addProjectButton.setDescription(Lang.get("dashboard.addProject.description"));
        headerLayout.addComponent(addProjectButton);

        addSortBox();
        addComponent(headerLayout);
    }

    private void addSortBox() {
        sortSelect = new ComboBox();
        sortSelect.addItem(SortOrder.DEADLINE);
        sortSelect.setItemCaption(SortOrder.DEADLINE,
                Lang.get("dashboard.sort." + SortOrder.DEADLINE.name().toLowerCase()));
        sortSelect.addItem(SortOrder.STATUS);
        sortSelect.setItemCaption(SortOrder.STATUS, Lang.get("dashboard.sort." + SortOrder.STATUS.name().toLowerCase()));

        sortSelect.setImmediate(true);
        sortSelect.setNullSelectionAllowed(false);

        sortSelect.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                presenter.sortProjects((SortOrder) event.getProperty().getValue());
            }
        });

        headerLayout.addComponent(sortSelect);
        headerLayout.setComponentAlignment(sortSelect, Alignment.BOTTOM_RIGHT);
    }

    private void addProjectsLayout() {
        projectsLayout = new CssLayout();
        projectsLayout.setSizeUndefined();
        projectsLayout.addStyleName(DashboardTheme.Dashboard.PROJECTS_LAYOUT);

        addComponent(projectsLayout);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        String dashboardName = event.getParameters();
        presenter.viewEntered(dashboardName);
        Page.getCurrent().setTitle(Lang.get("dashboard.title", dashboardName));
    }

    @Override
    public void detach() {
        presenter.viewExited();
        super.detach();
    }

    @Override
    public void addNewProject(Project project) {
        addProjectButton.setEnabled(false);
        disableShortcuts();

        addProjectLayout = new AddProjectLayout(project, presenter);
        addComponent(addProjectLayout, getComponentIndex(headerLayout) + 1);
    }

    @Override
    public void showProjects(Collection<Project> projects) {
        projectLayoutMap.clear();
        projectsLayout.removeAllComponents();

        for (Project project : projects) {
            addProject(project);
        }
    }

    private void addProject(Project project) {
        final ProjectStatusLayout statusLayout = new ProjectStatusLayout(project, presenter, navigate);
        projectLayoutMap.put(project, statusLayout);
        projectsLayout.addComponent(statusLayout, 0);
        new TextField().isImmediate();
    }

    @Override
    public void hideAddProjectEditor() {
        if (addProjectLayout != null) {
            removeComponent(addProjectLayout);
            addProjectLayout = null;
        }
        addProjectButton.setEnabled(true);
        addShortcuts();
    }

    @Override
    public void removeProject(Project project) {
        final Layout layout = projectLayoutMap.get(project);
        if (layout != null) {
            projectsLayout.removeComponent(layout);
            projectLayoutMap.remove(project);
        }
    }

    @Override
    public void showDashboardNameError() {
        Notification.show(Lang.get("dashboard.name.cannotBeEmpty"), Notification.Type.WARNING_MESSAGE);
        UI.getCurrent().getNavigator().navigateTo(SelectDashboardView.NAME);
    }

    @Override
    public void setSortOrder(SortOrder sortOrder) {
        sortSelect.select(sortOrder);
    }

    @Override
    public void setDashboardName(String dashboardName) {
        dashboardNameEditor.setValue(dashboardName);
    }

    private void addShortcuts() {
        addProjectButton.setClickShortcut(ShortcutAction.KeyCode.A);
        UI.getCurrent().focus();
    }

    private void disableShortcuts() {
        addProjectButton.removeClickShortcut();
    }
}

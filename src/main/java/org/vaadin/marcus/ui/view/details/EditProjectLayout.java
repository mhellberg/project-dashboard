package org.vaadin.marcus.ui.view.details;

import com.vaadin.ui.Button;
import com.vaadin.ui.VerticalLayout;
import org.vaadin.marcus.domain.Project;
import org.vaadin.marcus.ui.DashboardTheme;
import org.vaadin.marcus.ui.components.ProjectForm;
import org.vaadin.marcus.util.Lang;

/**
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public class EditProjectLayout extends VerticalLayout {

    private DetailsPresenter presenter;
    private ProjectForm projectForm;

    public EditProjectLayout(Project project, DetailsPresenter presenter) {
        this.presenter = presenter;
        setSpacing(true);

        addStyleName(DashboardTheme.INLINE_LAYOUT);
        addStyleName(DashboardTheme.Details.EDIT_PROJECT_LAYOUT);

        addForm(project);
    }

    private void addForm(Project project) {
        final ProjectForm.Config config = new ProjectForm.Config();
        config.setProject(project);
        config.setCancelListener(cancelListener);
        config.setCancelButtonCaption(Lang.get("general.cancel"));
        config.setOkButtonListener(saveListener);
        config.setOkButtonCaption(Lang.get("details.editSave"));
        config.setCommitFailMessage(Lang.get("details.editFailed"));
        projectForm = new ProjectForm(config);
        addComponent(projectForm);
    }


    private Button.ClickListener cancelListener = new Button.ClickListener() {
        @Override
        public void buttonClick(Button.ClickEvent event) {
            presenter.editCanceled();
        }
    };

    private Button.ClickListener saveListener = new Button.ClickListener() {
        @Override
        public void buttonClick(Button.ClickEvent event) {
            projectForm.commit();
            presenter.projectSaved();
        }
    };
}

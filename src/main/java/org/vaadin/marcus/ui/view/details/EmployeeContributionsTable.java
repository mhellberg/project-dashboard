package org.vaadin.marcus.ui.view.details;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.ui.Table;
import org.joda.time.DateTime;
import org.vaadin.marcus.domain.EmployeeInfo;
import org.vaadin.marcus.util.Lang;

import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public class EmployeeContributionsTable extends Table {
    public EmployeeContributionsTable(List<EmployeeInfo> employeeInfo) {
        setSizeFull();
        setContainerDataSource(new BeanItemContainer<EmployeeInfo>(EmployeeInfo.class, employeeInfo));
        setVisibleColumns("employeeName", "hours", "latestWHEntry");
        setColumnHeaders(Lang.get("details.table.employee.name"), Lang.get("details.table.hours"), Lang.get("details.table.latest.entry"));
        setConverter("latestWHEntry", new Converter<String, Date>() {

            @Override
            public Date convertToModel(String s, Class<? extends Date> aClass, Locale locale) throws ConversionException {
                return null;
            }

            @Override
            public String convertToPresentation(Date date, Class<? extends String> aClass, Locale locale) throws ConversionException {
                return Lang.getShortDate(new DateTime(date));
            }

            @Override
            public Class<Date> getModelType() {
                return Date.class;
            }

            @Override
            public Class<String> getPresentationType() {
                return String.class;
            }
        });
    }
}

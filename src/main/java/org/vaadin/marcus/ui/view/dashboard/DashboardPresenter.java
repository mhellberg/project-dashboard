package org.vaadin.marcus.ui.view.dashboard;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaadin.marcus.domain.Dashboard;
import org.vaadin.marcus.domain.Project;
import org.vaadin.marcus.ui.Presenter;
import org.vaadin.marcus.ui.view.DashboardChangedEvent;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Presenter for {@link DashboardView}s
 *
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public class DashboardPresenter extends Presenter {

    Logger logger = LoggerFactory.getLogger(getClass());

    protected Project currentProject;
    protected Dashboard dashboard;
    protected List<Project> projects;
    protected SortOrder currentSortOrder;

    @Inject
    Event<DashboardChangedEvent> dashboardChangedEventEvent;

    @Override
    public DashboardView getView() {
        return (DashboardView) super.getView();
    }

    @Override
    public void viewEntered(String parameters) {
        if (parameters == null || parameters.isEmpty()) {
            getView().showDashboardNameError();
        }

        dashboard = service.findOrCreateDashboard(parameters);

        getView().setDashboardName(dashboard.getDashboardName());
        setSortOrder();
        updateAllProjects();
    }

    private void setSortOrder() {
        if (dashboard.getSortOrder() != null) {
            currentSortOrder = dashboard.getSortOrder();
        } else {
            currentSortOrder = SortOrder.STATUS;
        }

        getView().setSortOrder(dashboard.getSortOrder());
    }

    @Override
    public void viewExited() {
        if (dashboard != null && !dashboard.getProjects().isEmpty()) {
            service.saveDashboard(dashboard);
        }
    }

    public void addProjectClicked() {
        currentProject = new Project();
        getView().addNewProject(currentProject);
    }

    public void projectAdded() {
        dashboard.addProject(currentProject);
        projects.add(currentProject);
        dashboard = service.saveDashboard(dashboard);

        service.updateProjectStatus(currentProject);
        currentProject = null;

        getView().hideAddProjectEditor();
        updateAllProjects();
    }

    public void addProjectAborted() {
        currentProject = null;
        getView().hideAddProjectEditor();
    }

    public void removeProject(Project project) {
        dashboard.removeProject(project);
        projects.remove(project);
        getView().removeProject(project);
    }

    protected void updateAllProjects() {
        projects = new ArrayList<Project>(dashboard.getProjects());
        service.updateProjectStatus(projects);
        sortProjects(currentSortOrder);
    }

    public void sortProjects(SortOrder order) {
        currentSortOrder = order;
        dashboard.setSortOrder(order);

        if (order == SortOrder.STATUS) {
            Collections.sort(projects, new Comparator<Project>() {
                @Override
                public int compare(Project project, Project project2) {
                    return project.getStatus().ordinal() - project2.getStatus().ordinal();
                }
            });
        } else if (order == SortOrder.DEADLINE) {
            Collections.sort(projects, new Comparator<Project>() {
                @Override
                public int compare(Project project, Project project2) {
                    return project2.getDeadline().compareTo(project.getDeadline());
                }
            });
        }
        getView().showProjects(projects);
    }

    public void renameDashboard(String newName) {
        dashboard.setDashboardName(newName);
        dashboard = service.saveDashboard(dashboard);
        dashboardChangedEventEvent.fire(new DashboardChangedEvent(newName));
    }
}

package org.vaadin.marcus.ui.view.select;

import com.vaadin.navigator.View;

/**
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public interface SelectDashboardView extends View {
    public static final String NAME = "select-dashboard";
}

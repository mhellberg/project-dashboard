package org.vaadin.marcus.ui.view;

/**
 * Event for signaling dashboard changes.
 *
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public class DashboardChangedEvent {

    private String dashboardName;

    public DashboardChangedEvent(String dashboardName) {
        this.dashboardName = dashboardName;
    }

    public String getDashboardName() {
        return dashboardName;
    }
}

package org.vaadin.marcus.ui.view.details;

import com.vaadin.event.LayoutEvents;
import com.vaadin.event.ShortcutAction;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;
import org.pegdown.PegDownProcessor;
import org.vaadin.marcus.domain.Project;
import org.vaadin.marcus.ui.DashboardTheme;
import org.vaadin.marcus.util.Lang;

/**
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public class NotesLayout extends CssLayout {

    private final PegDownProcessor pegDownProcessor = new PegDownProcessor();

    private final DetailsPresenter presenter;
    private Label noteContent;
    private VerticalLayout editorLayout;
    private LayoutEvents.LayoutClickListener layoutClickListener;

    public NotesLayout(Project project, DetailsPresenter presenter) {
        this.presenter = presenter;
        addStyleName(DashboardTheme.Details.NOTES_LAYOUT);

        initLayout(project, presenter);
    }

    private void initLayout(Project project, final DetailsPresenter presenter) {
        Label notesHeading = new Label(Lang.get("details.notes"));
        notesHeading.addStyleName(DashboardTheme.LABEL_H2);

        noteContent = new Label("", ContentMode.HTML);
        noteContent.addStyleName(DashboardTheme.Details.NOTE_CONTENT);
        String notes = project.getNotes();

        if (notes == null || notes.isEmpty()) {
            showNotes(Lang.get("details.addNotesInstructions"));
        } else {
            showNotes(notes);
        }

        addComponents(notesHeading, noteContent);

        layoutClickListener = new LayoutEvents.LayoutClickListener() {
            @Override
            public void layoutClick(LayoutEvents.LayoutClickEvent event) {
                presenter.editNotesClicked();
            }
        };
        addLayoutClickListener(layoutClickListener);
    }


    public void showNotes(String notes) {
        if (editorLayout != null) {
            removeComponent(editorLayout);
            editorLayout = null;
            addComponent(noteContent);
            addLayoutClickListener(layoutClickListener);
        }

        if (notes != null) {
            noteContent.setValue(pegDownProcessor.markdownToHtml(notes));
        }
    }

    public void editContent(String rawContent) {
        removeLayoutClickListener(layoutClickListener);
        removeComponent(noteContent);

        editorLayout = new VerticalLayout();
        editorLayout.setSpacing(true);
        editorLayout.addStyleName(DashboardTheme.Details.NOTES_EDITOR_LAYOUT);

        final TextArea editor = new TextArea();
        editor.focus();
        editor.setRows(10);
        editor.setWidth("100%");
        editor.setNullRepresentation("");
        editor.addStyleName(DashboardTheme.Details.NOTES_EDITOR);
        editor.setValue(rawContent);
        editorLayout.addComponent(editor);

        HorizontalLayout buttonsLayout = new HorizontalLayout();
        buttonsLayout.setSpacing(true);

        Button cancelButton = new Button(Lang.get("general.cancel"), new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                presenter.noteEditCanceled();
            }
        });

        Button saveButton = new Button(Lang.get("details.saveNotes"), new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                presenter.notesSaved(editor.getValue());
            }
        });


        saveButton.addStyleName(DashboardTheme.BUTTON_DEFAULT);

        cancelButton.setClickShortcut(ShortcutAction.KeyCode.ESCAPE);
        saveButton.setClickShortcut(ShortcutAction.KeyCode.ENTER, ShortcutAction.ModifierKey.ALT);

        buttonsLayout.addComponents(cancelButton, saveButton);
        buttonsLayout.setWidth("100%");
        buttonsLayout.setExpandRatio(cancelButton, 1.0f);
        buttonsLayout.setComponentAlignment(cancelButton, Alignment.TOP_RIGHT);

        editorLayout.addComponents(editor, buttonsLayout);
        addComponent(editorLayout);
    }
}

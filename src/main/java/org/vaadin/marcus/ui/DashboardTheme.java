package org.vaadin.marcus.ui;

import com.vaadin.ui.themes.Reindeer;

/**
 * Theme class for ProjectDashboard.
 *
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public class DashboardTheme extends Reindeer {

    public static final String THEME_NAME = "dashboard";

    public static final String ROOT_LAYOUT = "root-layout";
    public static final String LOGO_LABEL = "logo-label";
    public static final String HEADER_LAYOUT = "header-layout";
    public static final String CONTENT_LAYOUT = "content-layout";
    public static final String CONFIRM_WINDOW = "confirm-window";
    public static final String CONFIRMATION_QUESTION = "confirmation-window-question";
    public static final String STATUS_GRID = "status-grid";
    public static final String WARNING = "warning";
    public static final String PROJECT_FORM = "project-form";
    public static final String PROJECT_FORM_LABEL = "form-label";
    public static final String INLINE_LAYOUT = "inline-layout";
    public static final String CONTRACT_ID_FIELD = "contract-id-field";
    public static final String LABEL_ERROR = "error";

    /**
     * Styles used on
     * {@link org.vaadin.marcus.ui.view.select.SelectDashboardView}
     */
    public class Select {
        public static final String VIEW_STYLE = "select-dashboard-view";
        public static final String FORM_WRAPPER = "form-wrapper";
        public static final String HELP_LABEL = "help-label";
        public static final String WRAPPER_LAYOUT = "wrapper";
    }

    /**
     * Styles used on {@link org.vaadin.marcus.ui.view.dashboard.DashboardView}
     */
    public static class Dashboard {
        public static final String VIEW_STYLE = "dashboard-view";
        public static final String ADD_PROJECT_LAYOUT = "add-project-layout";
        public static final String ADD_WINDOW_HELP_LABEL = "help-label";
        public static final String FORM_FIELD = "form-field";
        public static final String ADD_PROJECT_BUTTONS_LAYOUT = "add-project-buttons";
        public static final String ADD_PROJECT_BUTTON = "add-project-button";
        public static final String CANCEL_PROJECT_BUTTON = "cancel-project-button";
        public static final String PROJECTS_LAYOUT = "projects-layout";
        public static final String HEADER = "header";
        public static final String PROJECT_NAME = "project-name";
        public static final String PROJECT_STATUS_LAYOUT = "project-status-layout";
        public static final String DELETE_BUTTON = "delete-button";
        public static final String DASHBOARD_NAME_BUTTON = "dashboard-name-button";
        public static final String DASHBOARD_NAME_EDITOR = "dashboard-name-editor";
    }

    /**
     * Styles used on {@link org.vaadin.marcus.ui.view.details.DetailsView}
     */
    public class Details {
        public static final String VIEW_NAME = "details-view";
        public static final String HEADER = "header";
        public static final String PROJECT_NAME = "project-name";
        public static final String CONTRIBUTION_CHART = "contribution-chart";
        public static final String DAILY_EFFORT_TIMELINE = "daily-effort";
        public static final String EDIT_PROJECT_LAYOUT = "edit-project";
        public static final String WRAPPER = "wrapper";
        public static final String NOTES_LAYOUT = "notes-layout";
        public static final String NOTES_EDITOR = "notes-editor";
        public static final String NOTES_EDITOR_LAYOUT = "notes-editor-layout";
        public static final String NOTE_CONTENT = "note-content";
    }
}

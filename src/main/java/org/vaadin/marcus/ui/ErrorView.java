package org.vaadin.marcus.ui;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

/**
 * Error view that is shown if incorrect URIs are accessed.
 *
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public class ErrorView extends VerticalLayout implements View {

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        removeAllComponents();
        addComponent(new Label("View '" + event.getViewName() + "'was not found. Check the URL."));
    }
}

package org.vaadin.marcus.ui.components;

import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.ShortcutAction;
import com.vaadin.ui.*;
import org.vaadin.marcus.domain.Project;
import org.vaadin.marcus.ui.DashboardTheme;
import org.vaadin.marcus.ui.view.dashboard.JodaDateTimeConverter;
import org.vaadin.marcus.util.Lang;

import java.util.Date;

/**
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public class ProjectForm extends VerticalLayout {

    private final FieldGroup fieldGroup;
    private Label errorLabel;
    private Config config;

    public ProjectForm(Config config) {
        this.config = config;

        addStyleName(DashboardTheme.PROJECT_FORM);
        setSpacing(true);

        fieldGroup = new FieldGroup();
        fieldGroup.setItemDataSource(new BeanItem<Project>(config.getProject()));

        addForm();
        addButtons();

    }

    private void addForm() {
        GridLayout formLayout = new GridLayout(4, 4) {
            @Override
            public void addComponent(Component component, int column1, int row1, int column2, int row2) throws OverlapsException, OutOfBoundsException {
                super.addComponent(component, column1, row1, column2, row2);
                setComponentAlignment(component, Alignment.TOP_LEFT);
            }

            @Override
            public void addComponent(Component component) {
                super.addComponent(component);
                setComponentAlignment(component, Alignment.TOP_LEFT);
            }
        };

        formLayout.setSpacing(true);
        formLayout.setColumnExpandRatio(1, 1);
        formLayout.setColumnExpandRatio(3, 1);
        formLayout.setWidth("100%");


        formLayout.addComponent(getLabel(Project.editableFields.name.name()));
        TextField nameField = getTextField(Project.editableFields.name.name());
        nameField.focus();
        formLayout.addComponent(nameField, 1, 0, 3, 0);

        formLayout.addComponent(getLabel(Project.editableFields.projectId.name()));
        formLayout.addComponent(getProjectIdField(Project.editableFields.projectId.name()));

        formLayout.addComponent(getLabel(Project.editableFields.contractIds.name()));
        formLayout.addComponent(getContractIdField(Project.editableFields.contractIds.name()));

        formLayout.addComponent(getLabel(Project.editableFields.budgetHours.name()));
        formLayout.addComponent(getTextField(Project.editableFields.budgetHours.name()));
        formLayout.newLine();

        formLayout.addComponent(getLabel(Project.editableFields.startDate.name()));
        formLayout.addComponent(getDateField(Project.editableFields.startDate.name()));

        formLayout.addComponent(getLabel(Project.editableFields.deadline.name()));
        formLayout.addComponent(getDateField(Project.editableFields.deadline.name()));

        addComponent(formLayout);
    }

    private TextField getTextField(String propertyId) {
        final TextField textField = new TextField();
        textField.setNullRepresentation("");
        textField.setRequired(true);
        textField.addStyleName(propertyId);
        textField.setWidth("100%");

        fieldGroup.bind(textField, propertyId);

        return textField;
    }


    private Field getProjectIdField(String propertyId) {
        final TextField projectIdField = new TextField();
        projectIdField.setNullRepresentation("");
        projectIdField.setWidth("100%");

        fieldGroup.bind(projectIdField, propertyId);
        return projectIdField;
    }

    private Field getContractIdField(String propertyId) {
        TextField field = new TextField();
        field.addStyleName(DashboardTheme.CONTRACT_ID_FIELD);
        field.setWidth("100%");
        field.setConverter(new IntegerSetConverter());
        fieldGroup.bind(field, propertyId);
        return field;
    }

    private Label getLabel(String propertyId) {
        final Label label = new Label(Lang.get("dashboard.projectform.field." + propertyId));
        label.setWidth("100px");
        label.addStyleName(DashboardTheme.PROJECT_FORM_LABEL);
        return label;
    }

    private DateField getDateField(String propertyId) {

        final DateField dateField = new DateField();
        dateField.addStyleName(DashboardTheme.Dashboard.FORM_FIELD);
        dateField.addStyleName(propertyId);
        dateField.setRequired(true);
        dateField.setConverter(new JodaDateTimeConverter());
        dateField.setValue(new Date());
        dateField.setWidth("100%");

        fieldGroup.bind(dateField, propertyId);
        return dateField;
    }

    public boolean commit() {
        try {
            fieldGroup.commit();
            return true;
        } catch (FieldGroup.CommitException e) {
            showError();
            e.printStackTrace();
            return false;
        }
    }

    private void addButtons() {
        final HorizontalLayout buttonsLayout = new HorizontalLayout();
        buttonsLayout.addStyleName(DashboardTheme.Dashboard.ADD_PROJECT_BUTTONS_LAYOUT);
        buttonsLayout.setSpacing(true);
        buttonsLayout.setWidth("100%");
        buttonsLayout.addStyleName(DashboardTheme.Dashboard.ADD_PROJECT_BUTTON);

        final Button okButton = new Button(config.getOkButtonCaption(), config.getOkButtonListener());
        okButton.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        okButton.addStyleName(DashboardTheme.BUTTON_DEFAULT);
        okButton.addStyleName(DashboardTheme.Dashboard.ADD_PROJECT_BUTTON);

        final Button cancelButton = new Button(config.cancelButtonCaption, config.getCancelListener());
        cancelButton.setClickShortcut(ShortcutAction.KeyCode.ESCAPE);
        cancelButton.addStyleName(DashboardTheme.Dashboard.CANCEL_PROJECT_BUTTON);

        buttonsLayout.addComponents(cancelButton, okButton);
        buttonsLayout.setComponentAlignment(cancelButton, Alignment.TOP_RIGHT);
        buttonsLayout.setExpandRatio(cancelButton, 1);

        addComponent(buttonsLayout);
        setComponentAlignment(buttonsLayout, Alignment.BOTTOM_CENTER);
    }


    private void showError() {
        if (errorLabel == null) {
            errorLabel = new Label();
            errorLabel.setValue(config.getCommitFailMessage());
            errorLabel.addStyleName(DashboardTheme.LABEL_ERROR);
        }
        addComponent(errorLabel, 0);
    }


    public static class Config {
        private Project project;
        private Button.ClickListener cancelListener;
        private String cancelButtonCaption;
        private Button.ClickListener okButtonListener;
        private String okButtonCaption;
        private String commitFailMessage;

        public Project getProject() {
            return project;
        }

        public void setProject(Project project) {
            this.project = project;
        }

        public Button.ClickListener getCancelListener() {
            return cancelListener;
        }

        public void setCancelListener(Button.ClickListener cancelListener) {
            this.cancelListener = cancelListener;
        }

        public String getCancelButtonCaption() {
            return cancelButtonCaption;
        }

        public void setCancelButtonCaption(String cancelButtonCaption) {
            this.cancelButtonCaption = cancelButtonCaption;
        }

        public Button.ClickListener getOkButtonListener() {
            return okButtonListener;
        }

        public void setOkButtonListener(Button.ClickListener okButtonListener) {
            this.okButtonListener = okButtonListener;
        }

        public String getOkButtonCaption() {
            return okButtonCaption;
        }

        public void setOkButtonCaption(String okButtonCaption) {
            this.okButtonCaption = okButtonCaption;
        }

        public String getCommitFailMessage() {
            return commitFailMessage;
        }

        public void setCommitFailMessage(String commitFailMessage) {
            this.commitFailMessage = commitFailMessage;
        }
    }
}

package org.vaadin.marcus.ui.components;

import com.google.common.base.Joiner;
import com.vaadin.data.util.converter.Converter;

import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

/**
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public class IntegerSetConverter implements Converter<String, Set> {

    @Override
    public Set convertToModel(String value, Class<? extends Set> targetType, Locale locale) throws ConversionException {
        if (value == null) {
            return new HashSet<Integer>();
        }
        String cleaned = value.replace(" ", "");

        HashSet<Integer> set = new HashSet<Integer>();

        for (String string : cleaned.split(",")) {
            try {
                set.add(Integer.parseInt(string));
            } catch (NumberFormatException nfe) {
                // skip invalid entries
            }
        }

        return set;
    }

    @Override
    public String convertToPresentation(Set value, Class<? extends String> targetType, Locale locale) throws ConversionException {
        return Joiner.on(", ").join(value);
    }

    @Override
    public Class<Set> getModelType() {
        return Set.class;
    }

    @Override
    public Class<String> getPresentationType() {
        return String.class;
    }
}

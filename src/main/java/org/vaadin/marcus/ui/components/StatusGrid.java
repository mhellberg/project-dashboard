package org.vaadin.marcus.ui.components;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.ProgressBar;
import org.vaadin.marcus.domain.Project;
import org.vaadin.marcus.ui.DashboardTheme;
import org.vaadin.marcus.util.Lang;

/**
 * Displays project status in hours and calendar time.
 *
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public class StatusGrid extends GridLayout {

    private Project project;

    public StatusGrid(Project project) {
        super(3, 2);
        this.project = project;
        setColumnExpandRatio(1, 1.0f);
        setWidth("100%");
        setSpacing(true);
        addStyleName(DashboardTheme.STATUS_GRID);

        addHoursInfo();
        addTimeInfo();
    }


    private void addHoursInfo() {
        final Label hoursUsedLabel = new Label(Lang.get("dashboard.project.hoursUsed"));
        hoursUsedLabel.setWidth("80px");

        final ProgressBar hoursProgress = new ProgressBar();
        hoursProgress.setWidth("100%");
        hoursProgress.setValue(getSanitizedProgress(project.getHourProgress()));
        setWarningStyleIfNeeded(hoursProgress, project.getHourProgress());

        final Label hoursTextLabel = new Label(project.getUsedHours() + "/" + project.getBudgetHours() + Lang.get("dashboard.project.hoursUnit"));
        hoursTextLabel.setWidth("120px");

        addComponents(hoursUsedLabel, hoursProgress, hoursTextLabel);
        setComponentAlignment(hoursProgress, Alignment.MIDDLE_CENTER);
    }

    private void addTimeInfo() {
        final Label timeUsedLabel = new Label(Lang.get("dashboard.project.timeUsed"));
        timeUsedLabel.setWidth("100px");

        final ProgressBar timeProgress = new ProgressBar();
        timeProgress.setWidth("100%");
        timeProgress.setValue(getSanitizedProgress(project.getTimeProgress()));
        setWarningStyleIfNeeded(timeProgress, project.getTimeProgress());

        final Label deadlineLabel = new Label();
        deadlineLabel.setWidth("120px");

        if (project.getDeadlineJoda().isAfterNow()) {
            deadlineLabel.setValue(Lang.get("dashboard.project.endsOn", Lang.getShortDate(project.getDeadlineJoda())));
        } else {
            deadlineLabel.setValue(Lang.get("dashboard.project.endedOn", Lang.getShortDate(project.getDeadlineJoda())));
        }
        addComponents(timeUsedLabel, timeProgress, deadlineLabel);
        setComponentAlignment(timeProgress, Alignment.MIDDLE_CENTER);
    }

    private void setWarningStyleIfNeeded(ProgressBar progressIndicator, float progress) {
        if (progress > 1) {
            progressIndicator.addStyleName(DashboardTheme.WARNING);
        }
    }

    /**
     * Progress may be over 100% if project is over budget. Progress indicator does not
     * like percentages over 100%, so clean up anyting more than that.
     */
    protected float getSanitizedProgress(float progress) {
        return progress > 1 ? 1 : progress;
    }
}

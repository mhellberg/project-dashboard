package org.vaadin.marcus.ui.components;

import com.vaadin.event.ShortcutAction;
import com.vaadin.ui.*;
import org.vaadin.marcus.ui.DashboardTheme;
import org.vaadin.marcus.util.Lang;

/**
 * Window for confirming user actions.
 *
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public class ConfirmDialog extends Window {

    public ConfirmDialog(String caption, String question, final Runnable action) {
        addStyleName(DashboardTheme.CONFIRM_WINDOW);
        addStyleName(DashboardTheme.WINDOW_OPAQUE);

        setWidth("250px");
        setHeight("120px");
        center();
        setModal(true);
        setResizable(false);
        setCaption(caption);
        setCloseShortcut(ShortcutAction.KeyCode.ESCAPE);

        final VerticalLayout layout = new VerticalLayout();
        layout.setSizeFull();
        layout.setMargin(true);
        layout.setSpacing(true);
        setContent(layout);

        final Label questionLabel = new Label(question);
        questionLabel.addStyleName(DashboardTheme.CONFIRMATION_QUESTION);
        layout.addComponent(questionLabel);
        layout.setExpandRatio(questionLabel, 1.0f);
        addButtons(action, layout);

    }

    private void addButtons(final Runnable action, VerticalLayout layout) {
        final HorizontalLayout buttonsLayout = new HorizontalLayout();
        buttonsLayout.setWidth("100%");
        buttonsLayout.setSpacing(true);

        final Button cancelButton = new Button(Lang.get("general.cancel"), new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                close();
            }
        });

        final Button okButton = new Button(Lang.get("general.ok"), new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                action.run();
                close();
            }
        });

        okButton.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        okButton.addStyleName(DashboardTheme.BUTTON_DEFAULT);

        cancelButton.focus();

        buttonsLayout.addComponents(cancelButton, okButton);
        buttonsLayout.setComponentAlignment(cancelButton, Alignment.MIDDLE_RIGHT);
        buttonsLayout.setExpandRatio(cancelButton, 1.0f);
        layout.addComponent(buttonsLayout);
    }
}

package org.vaadin.marcus.ui;

import com.vaadin.cdi.UIScoped;
import com.vaadin.ui.UI;

import java.io.Serializable;
import java.util.Stack;

/**
 * Maintains a view stack with the current view on top.
 *
 * @author Marcus Hellberg (marcus@vaadin.com)
 */

@UIScoped
public class Navigate implements Serializable {

    private Stack<String> viewStack;

    /**
     * Updates the view stack.
     * Current view is always left on top of the stack.
     * Does not allow duplicates.
     *
     * @param viewAndParameters
     */
    public void navigatedTo(String viewAndParameters) {
        final Stack<String> viewStack = getViewStack();
        if (viewStack.contains(viewAndParameters)) {
            while (!viewStack.peek().equals(viewAndParameters)) {
                viewStack.pop();
            }
        } else {
            viewStack.push(viewAndParameters);
        }
    }

    /**
     * Navigates to a given url and parameters.
     *
     * @param viewAndParameters
     */
    public void to(String viewAndParameters) {
        UI.getCurrent().getNavigator().navigateTo(viewAndParameters);
    }

    /**
     * @return true if there is a previous view to go to
     */
    public boolean canGoBack() {
        return getViewStack().size() > 1;
    }

    /**
     * Navigates back if canGoBack() returns true.
     * Current view is always left on top of the stack.
     */
    public void back() {
        if (canGoBack()) {
            final Stack<String> viewStack = getViewStack();
            viewStack.pop(); // remove the topmost
            to(viewStack.peek()); // leave current on stack
        }
    }

    /**
     * Returns the view stack.
     * Will initialize view stack if one does not exist.
     *
     * @return view
     */
    protected Stack<String> getViewStack() {

        if (viewStack == null) {
            viewStack = new Stack<String>();
        }

        return viewStack;
    }
}

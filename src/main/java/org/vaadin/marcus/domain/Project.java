package org.vaadin.marcus.domain;

import org.joda.time.DateTime;
import org.joda.time.Days;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.*;

/**
 * Domain object representing a single project. Belongs to a {@link Dashboard}.
 *
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
@Entity
public class Project extends AbstractEntity {

    public static final double SAFETY_MARGIN = 0.1;

    private String name;

    @ElementCollection
    private Set<Integer> contractIds = new HashSet<Integer>();
    @NotNull
    private Integer projectId;
    private Integer budgetHours;
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Temporal(TemporalType.DATE)
    private Date deadline;
    private Integer usedHours;
    private String notes;


    public enum editableFields {
        name, projectId, contractIds, budgetHours, startDate, deadline
    }

    public enum Status {
        OK, WARNING, PROBLEM
    }

    @Transient
    private List<EmployeeInfo> employeeInfo = new LinkedList<EmployeeInfo>();


    // Getters and setters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Integer> getContractIds() {
        return contractIds;
    }

    public void setContractIds(Set<Integer> contractIds) {
        this.contractIds = contractIds;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public Integer getBudgetHours() {
        return budgetHours;
    }

    public void setBudgetHours(Integer budgetHours) {
        this.budgetHours = budgetHours;
    }

    public Date getStartDate() {
        return startDate;
    }

    public DateTime getStartDateJoda() {
        return new DateTime(getStartDate());
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getDeadline() {
        return deadline;
    }

    public DateTime getDeadlineJoda() {
        return new DateTime(getDeadline());
    }

    public Integer getUsedHours() {
        return usedHours;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public void setUsedHours(Integer usedHours) {
        this.usedHours = usedHours;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }


    public List<EmployeeInfo> getEmployeeInfo() {
        return employeeInfo;
    }

    public void setEmployeeInfo(List<EmployeeInfo> employeeInfo) {
        this.employeeInfo = employeeInfo;
    }


// Business logic

    /**
     * @return the percentage of used hours
     */
    public float getHourProgress() {
        return getUsedHours() / (float) getBudgetHours();
    }

    /**
     * @return the percentage of time used
     */
    public float getTimeProgress() {
        return 1 - (getDaysRemaining() / (float) getProjectDuration());
    }

    /**
     * @return the number of days remaining in the project
     */
    public int getDaysRemaining() {
        return Days.daysBetween(new DateTime().toDateMidnight(), getDeadlineJoda().toDateMidnight()).getDays();
    }

    /**
     * @return the total duration of the project in days
     */
    public int getProjectDuration() {
        return Days.daysBetween(getStartDateJoda().toDateMidnight(), getDeadlineJoda().toDateMidnight()).getDays();
    }

    public Status getStatus() {
        final float hourProgress = getHourProgress();
        final float timeProgress = getTimeProgress();

        if (hourProgress > 1 || timeProgress > 1 || hourProgress - timeProgress > SAFETY_MARGIN) {
            return Status.PROBLEM;
        } else if (hourProgress - timeProgress > 0) {
            return Status.WARNING;
        } else {
            return Status.OK;
        }
    }


}

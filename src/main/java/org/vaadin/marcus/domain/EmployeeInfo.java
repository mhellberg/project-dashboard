package org.vaadin.marcus.domain;

import java.util.Date;

/**
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public class EmployeeInfo {

    public String employeeName;
    public Double hours;
    public Date latestWHEntry;

    public EmployeeInfo(String employeeName, double hours, java.sql.Date latestWHEntry) {
        this.employeeName = employeeName;
        this.hours = hours;
        this.latestWHEntry = latestWHEntry;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public Double getHours() {
        return hours;
    }

    public void setHours(Double hours) {
        this.hours = hours;
    }

    public Date getLatestWHEntry() {
        return latestWHEntry;
    }

    public void setLatestWHEntry(Date latestWHEntry) {
        this.latestWHEntry = latestWHEntry;
    }
}

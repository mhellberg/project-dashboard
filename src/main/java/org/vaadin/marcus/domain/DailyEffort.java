package org.vaadin.marcus.domain;

import java.util.Date;

/**
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public class DailyEffort {
    private Date date;
    private Double hours;

    public DailyEffort(Date date, double hours) {
        this.date = date;
        this.hours = hours;
    }


    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getHours() {
        return hours;
    }

    public void setHours(Double hours) {
        this.hours = hours;
    }
}

package org.vaadin.marcus.domain;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public class Repository {

    @PersistenceContext(name = "dashboard")
    EntityManager em;


    public Dashboard findDashboard(Long id) {
        return em.find(Dashboard.class, id);
    }

    public Dashboard findDashdobard(String name) {
        TypedQuery<Dashboard> query = em.createNamedQuery(Dashboard.FIND_BY_NAME, Dashboard.class);
        query.setParameter("name", name);
        return getSingleResultOrNull(query);
    }

    public void removeDashboard(Dashboard dashboard) {
        em.remove(dashboard);
    }

    @TransactionAttribute(TransactionAttributeType.MANDATORY)
    public <E extends AbstractEntity> E save(E entity) {
        E saved;
        if (entity.isPersistent()) {
            saved = em.merge(entity);
        } else {
            em.persist(entity);
            saved = entity;
        }
        em.flush();
        return saved;
    }


    protected Dashboard getSingleResultOrNull(TypedQuery<Dashboard> query) {
        Dashboard found = null;

        try {
            found = query.getSingleResult();
        } catch (NoResultException derp) {
            // herp derp jpa derp
        }

        return found;
    }

    public Project findProject(long id) {
        return em.find(Project.class, id);
    }
}

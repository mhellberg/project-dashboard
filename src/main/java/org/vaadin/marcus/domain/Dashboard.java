package org.vaadin.marcus.domain;

import org.vaadin.marcus.ui.view.dashboard.SortOrder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.vaadin.marcus.domain.Dashboard.FIND_BY_NAME;

/**
 * Domain object for representing a Dashboard.
 * Dashboards are identified by a name and contain 0..* {@link Project}s.
 *
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
@Entity
@NamedQuery(name = FIND_BY_NAME, query = "select d from Dashboard d where d.dashboardName=:name")
public class Dashboard extends AbstractEntity {
    public static final String FIND_BY_NAME = "dashboard.findByName";

    @NotNull
    private String dashboardName;

    @Enumerated(EnumType.STRING)
    private SortOrder sortOrder;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<Project> projects = new HashSet<Project>();

    public String getDashboardName() {
        return dashboardName;
    }

    public void setDashboardName(String dashboardName) {
        this.dashboardName = dashboardName;
    }

    public SortOrder getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(SortOrder sortOrder) {
        this.sortOrder = sortOrder;
    }

    public void addProject(Project project) {
        projects.add(project);
    }

    public void removeProject(Project project) {
        projects.remove(project);
    }

    public Set<Project> getProjects() {
        return Collections.unmodifiableSet(projects);
    }
}

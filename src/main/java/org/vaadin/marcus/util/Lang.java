package org.vaadin.marcus.util;

import com.vaadin.server.VaadinSession;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.MessageFormat;
import java.util.*;

/**
 * Helper class that provides translations to the UI. Translations are loaded
 * from Java properties files.
 *
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public class Lang {

    private static final Map<Locale, ResourceBundle> translations = new HashMap<Locale, ResourceBundle>();

    /**
     * First locale given will be considered default, and is used as a fallback
     * if current locale is not set. Add locales here after adding a translation
     * file in the <code>resources</code> folder.
     */
    private static final List<Locale> SUPPORTED_LOCALES = Arrays
            .asList(new Locale("en", "US"));

    private static final String TRANSLATION_BASE_NAME = "dashboard";

    static {
        loadTranslations();
    }

    /**
     * Loads all translation files through the class loader. Will load all
     * {@link Locale}s specified in {@link #SUPPORTED_LOCALES}.
     * <p/>
     * Translation files must follow naming convention
     * {TRANSLATION_BASE_NAME}_en_US.properties
     */
    private static void loadTranslations() {
        ClassLoader classLoader = Lang.class.getClassLoader();
        for (Locale locale : SUPPORTED_LOCALES) {
            try {
                translations.put(locale, ResourceBundle.getBundle(
                        TRANSLATION_BASE_NAME, locale, classLoader));
                System.out.println("Loaded translation file for locale " + locale);
            } catch (MissingResourceException e) {
                System.err.println("Failed to load translation file for locale  " + locale);
            }
        }
    }

    /**
     * Resolves a given translationKey to a corresponding string, using the
     * currently set {@link Locale}. If arguments are given, they are
     * substituted into the string in the order given.
     *
     * @param translationKey Key that corresponds to translation, eg. login.username
     * @param args           Additional arguments that should be substituted in place of
     *                       numeric placeholders (like {0})
     * @return Translated string, if found. Otherwise the translation key,
     *         prepended with an exclamation mark will be returned.
     */
    public static String get(String translationKey, Object... args) {
        return get(translationKey, getLocale(), args);
    }

    public static String get(String translationKey, Locale locale,
                             Object... args) {
        try {
            String translated = getBundle(locale).getString(translationKey);

            if (args != null && args.length > 0) {
                translated = MessageFormat.format(translated, args);
            }

            return translated;
        } catch (MissingResourceException e) {
            System.err.println("Missing translation for [" + translationKey + "]");
            return "!" + translationKey;
        }
    }

    /**
     * Return translation bundle for current {@link Locale}.
     *
     * @return
     */
    private static ResourceBundle getBundle(Locale locale) {
        return translations.get(locale);
    }

    /**
     * @return Returns the current active {@link Locale} if set, otherwise the
     *         first {@link Locale} in {@link #SUPPORTED_LOCALES}.
     */
    private static Locale getLocale() {
        Locale locale = VaadinSession.getCurrent().getLocale();

        if (locale == null) {
            locale = SUPPORTED_LOCALES.get(0);
        }
        return locale;
    }

    public static String getShortDate(DateTime date) {
        return getShortDate(date, getLocale());
    }

    public static String getShortDate(DateTime date, Locale locale) {
        final DateTimeFormatter formatter = DateTimeFormat.shortDate();
        return formatter.print(date);
    }
}

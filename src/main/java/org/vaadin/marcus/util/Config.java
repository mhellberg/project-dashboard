package org.vaadin.marcus.util;

import java.util.ResourceBundle;

/**
 * Accessor class for getting data from the application's configuration file.
 *
 * @author Marcus Hellberg (marcus@vaadin.com)
 */
public class Config {

    private static final String CONFIG_NAME = "settings";
    private static ResourceBundle configBundle;

    static {
        loadConfig();
    }

    private static void loadConfig() {
        configBundle = ResourceBundle.getBundle(CONFIG_NAME);
    }

    public static String get(String key) {
        return configBundle.getString(key);
    }

    public static Integer getInt(String key) {
        return Integer.parseInt(get(key));
    }

    public static boolean getBoolean(String key) {
        return get(key).equalsIgnoreCase("true");
    }

}

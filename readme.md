# Project Dashboard

The project is maven based, so it should be simple to get running in any IDE. 

## Running
The project will require two JDBC DataSources to be configured in the server:
* jdbc/wh - for accessing the WH database
* jdbc/dashboard - for accessing the Project Dashboard database

Build:
mvn package

Deploy package to any JavaEE Web profile supporting application server. 